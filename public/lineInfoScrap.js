// get the lineas bus info file
var lineasBus;
$.getJSON( "data/scrapLineasBusBurgos.json", function( data ) {
  lineasBus = data.lineasBus;
});

map.on('popupopen', function(e) {
  var classname = document.getElementsByClassName("boxLine");

  var myFunction = function() {
      var text = this.innerText;
      var index = lineasBus.map(function(d) { return d['numero']; }).indexOf(text)

      // var titleTag = '<p>' + lineasBus[index].name + '</p>'
      var imgTag =  '<img src="data/lineas-jpg/'+ text +'.jpg" alt="'+text+' jpg" width= 90%;">'
      document.getElementById("lineInfo").innerHTML = imgTag
  };

  for(var i=0;i<classname.length;i++){
      classname[i].addEventListener("click", myFunction, false);
  }

});

// not working because aytoburgos web only has http!!
// function getLineHTMLInfo(lineName) {
//   var baseUrl = 'http://www.aytoburgos.es/movilidad-y-transporte/mapa-lineas-autobus/';
//   var htmlInfo;
//   $.get(baseUrl + lineName, function(response) {
//     var htmlInfo = $(response).find('.content');
//     console.log(htmlInfo)
//     // return htmlInfo
//     document.getElementById(lineInfo).innerHTML = htmlInfo
//
//   });
// }
