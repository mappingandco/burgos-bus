function getColor(r) {
  return r == 'L01' ? '#e10021' :
    r == 'L02' ? '#9f3e8a' :
    r == 'L03' ? '#78b467' :
    r == 'L03B' ? '#499e2b' :
    r == 'L04' ? '#f4a963' :
    r == 'L05' ? '#fdcc00' :
    r == 'L06' ? '#88c2e2' :
    r == 'L07' ? '#5f4887' :
    r == 'L08' ? '#005a29' :
    r == 'L09' ? '#5b303e' :
    r == 'L10' ? '#be0243' :
    r == 'L11' ? '#f1da00' :
    r == 'L12' ? '#a4131f' :
    r == 'L13' ? '#4ac0e7' :
    r == 'L14' ? '#ddb82e' :
    r == 'L15' ? '#006fb0' :
    r == 'L16' ? '#b0bc93' :
    r == 'L17' ? '#ef9fbe' :
    r == 'L18' ? '#a1a012' :
    r == 'L19' ? '#ba0068' :
    r == 'L20' ? '#e96b0f' :
    r == 'L21' ? '#6ec3c9' :
    r == 'L22' ? '#ca4e19' :
    r == 'L24' ? '#004f90' :
    r == 'L25' ? '#20296d' :
    r == 'L26' ? '#ae7dad' :
    r == 'L27' ? '#659726' :
    r == 'L39' ? '#dc0078' :
    r == 'L43' ? '#009799' :
    r == 'L44' ? '#17171d' :
    r == 'L45' ? '#c9d000' :
    r == 'L80' ? '#ffeb00' :
    r == 'L81' ? '#d8b95e' :
              '#17171d';
}
function styleLineas(feature) {
  return {
      color: getColor(feature.properties.ref),
      weight: 2,
      opacity: 1,
      // dashArray: '3',
      // fillOpacity: 0.7
  };
}
