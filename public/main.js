
// initialize the map
var map = L.map('map').setView([42.344335, -3.69380950], 15);

function buslineFilter(feature) {
  if (feature.geometry.type === "LineString") return true
}
var CartoDB_Positron = L.tileLayer('https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="https://carto.com/attributions">CARTO</a>',
	subdomains: 'abcd',
	maxZoom: 19
});

// load the tile layer
CartoDB_Positron.addTo(map);
$('#mySpinner').addClass('spinner');

$.getJSON("data/lineasBus.geojson",function(data){
  var busIcon = L.icon({
    iconUrl: 'data/iconfinder_Aiga_bus_134138.png',
    iconSize: [19,24],
    popupAnchor: [0, -12]
  });

  L.geoJson(data, {
    pointToLayer: function(feature,latlng){
      // add maker only on public_transport = platform not in route stops
      if (feature.properties.public_transport == 'platform') {
        busMarker = L.marker(latlng,{icon: busIcon});
        return busMarker;
      }
    },
    style: styleLineas
      // filter: buslineFilter
  }).bindPopup(function (layer) {
    if (layer.feature.geometry.type === "Point") {
      busStopName = layer.feature.properties.name;
      popupContent = "<i class='fas fa-bus'></i> "+busStopName+"<hr>Lineas:<br/>";
      busStopRelations = layer.feature.properties["@relations"];
      var busLines = [];
      var popupLines = "<div class='lineNumber'>";

      for (var i = 0; i < busStopRelations.length; i++) {
        busLines.push({
          busLineFrom: busStopRelations[i].reltags.from,
          busLineTo:  busStopRelations[i].reltags.to,
          busLineRef: busStopRelations[i].reltags.ref,
          busLineName: busStopRelations[i].reltags.name
        });
        // popupLines += "<span class='boxLine'>" + busLines[i].busLineRef + "</span>";
        popupLines += "<span class='boxLine'><a href='#ex1' rel='modal:open'>";
        popupLines += busLines[i].busLineRef + "</a></span>";
      }
      popupLines += '</div>'
      popupContent += popupLines;

      return popupContent;

    } else {
      return layer.feature.properties.name;

    }
  }).addTo(map);
  $('#mySpinner').removeClass('spinner');
});
