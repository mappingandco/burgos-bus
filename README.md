# BURGOS bus


## Descargar datos paradas y rutas desde OpenStreetMap:


Usaremos la API de overpass-turbo

https://overpass-turbo.eu/#


```
[out:json][bbox:42.2829,-3.8614,42.4230,-3.5421][timeout:25];
(
  node["public_transport"="platform"];
  //relation["route"="bus"]["ref"="L13"];
  relation["route"="bus"];
);
out body;
>;
out skel qt;
```
## Folleto lineas autobus ayto Burgos

http://www.aytoburgos.es/archivos/movilidad-y-transporte/articulo/documentos/autobusesburgos.pdf


## todo

https://github.com/Leaflet/Leaflet.fullscreen

https://opencagedata.com/tutorials/geocode-in-leaflet
