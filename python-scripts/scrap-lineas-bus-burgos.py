#!/usr/bin/python

# conda activate webscrap-env
import os
import requests
from bs4 import BeautifulSoup
import json
import datetime

# get current working directory (path/to/burgos-bus/python-scripts)
cwd = os.getcwd()
# change current dir to parent folder (path/to/burgos-bus)
os.chdir(os.path.abspath(os.path.join(cwd, os.pardir)))


aytoBaseURL= "http://www.aytoburgos.es"
lineasBusUrl = "/movilidad-y-transporte/mapa-lineas-autobus/"

page = requests.get(aytoBaseURL + lineasBusUrl)

soup = BeautifulSoup(page.content, 'html.parser')

# get num and title in lineas bus table:
bus_Lines_Numero = soup.find_all(class_="views-field-field-lin-numero-value")
bus_Lines_title = soup.find_all(class_="views-field-title")

lineasNumList = []
lineasTitleList = []
lineasURLList = []

for line_num in bus_Lines_Numero:
    lineasNumList.append(line_num.get_text().strip())
for line_title in bus_Lines_title:
    lineasTitleList.append(line_title.get_text().strip())
    for a in line_title.find_all("a", href=True):
        lineasURLList.append(a["href"])

lineasList = []
for index in range(len(lineasNumList)-1):
    lineasNumList[index]
    lineasTitleList[index]
    lineasURLList[index]
    linea = {
        "numero": lineasNumList[index+1],
        "url": lineasURLList[index+1],
        "name": lineasTitleList[index+1],
        "jpg_url":""
    }
    lineasList.append(linea)


# #######################
# scrap individual lines
# #######################

for index in range(len(lineasList)):

    ind_page = requests.get(aytoBaseURL + lineasList[index]["url"])
    print("getting linea: "+ lineasList[index]["numero"])
    print("status:", ind_page.status_code)
    soup = BeautifulSoup(ind_page.content, 'html.parser')
    # get ficheros adjuntos data:
    ficheros = soup.find_all(class_= "filefield-file")

    for item in ficheros:
        for a in item.find_all('a', href=True):
            if a['type'][:5] == 'image':
                jpg_url =a['href']

    lineasList[index]["jpg_url"] = jpg_url

# save data in a json file
now = datetime.datetime.now()
with open('public/data/scrapLineasBusBurgos.json', 'w', encoding='utf-8') as outfile:
    json.dump({'lineasBus': lineasList, 'creado':now.strftime(" %d %b %Y %H:%M:%S")}, outfile, ensure_ascii=False)


# Download the jpg file for each bus line:
for index in range(len(lineasList)):

    url = lineasList[index]["jpg_url"]
    r = requests.get(url, allow_redirects=True)
    if r.status_code == 200:
        # with open("public/data/lineas-jpg/"+lineasList[index]["numero"]+".jpg", 'wb') as file:
        #     file.write(r.content)
        open("public/data/lineas-jpg/"+lineasList[index]["numero"]+".jpg", 'wb').write(r.content)

# print('--------------------------------------------')
# clasesArray = ["numero","origen","destino", "zona", "horario"]
# linData = []
# zonas = []
# for clase in clasesArray:
#     field = soup.find_all(class_= "field-field-lin-" + clase)
#
#     if clase == "zona":
#         for fieldItem in field[0].find_all(class_="field-item"):
#             zonas.append(fieldItem.get_text().strip())
#         linData.append(zonas)
#
#     else:
#         fieldItem = field[0].find_all(class_="field-item")
#         if clase == "horario":
#             # print(fieldItem)
#             # linData.append(fieldItem[0])
#             i=0
#             for stuff in field[0].find_all('p'):
#                 print(stuff)
#                 print('------',i)
#                 i+=1
#
#             # find <u> inside a <p> .select("p u")
#
#         else:
#             linData.append(fieldItem[0].get_text().strip())
#
#
# print(linData)
